package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @program :       battleship
 * @description:    Class of BattileShip's Board
 **/
public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    private final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    private final HashSet<Coordinate> enemyMisses;
    private HashMap<Coordinate,T> enemyHit;
    final T missInfo;

    /**
    * @Description: Default constructor of BattleShipBoard. Constraint the value of passed width and height and the missInfo
    * @Param:       width, height, missInfo
    * @throws       IllegalArgumentException if the width or height is smaller than 0.
    * @return:
    **/
    public BattleShipBoard(int width, int height, T missInfo) {
        this.missInfo = missInfo;
        if (width <= 0) {
            throw new IllegalArgumentException("edu.duke.cq26.battleship.BattleShipBoard's width must be positive but is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("edu.duke.cq26.battleship.BattleShipBoard's height must be positive but is " + height);
        }
        this.width = width;
        this.height = height;
        this.myShips = new ArrayList<>();
        this.placementChecker = new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null));
        this.enemyMisses = new HashSet<>();
        this.enemyHit = new HashMap<>();
    }


    /** Getter and Setters **/
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ArrayList<Ship<T>> getMyShips() {
        return myShips;
    }

    public HashSet<Coordinate> getEnemyMisses() {
        return enemyMisses;
    }

    /**
    * @Description: Add the passed ship into Board. Operation Add operation is checked no collision and no out of bound first
    * @Param:       Ship<T> toAdd
    * @return:      String that describe problems in ships' placement (if no problem, return null)
    **/
    public String tryAddShip(Ship<T> toAdd){
        String descr = this.placementChecker.checkPlacement(toAdd, this);
        if(descr == null){
            myShips.add(toAdd);
        }
        return descr;
    }

    /**
     * @Description: Move one ship from original location to another location.
     *               Try to remove original ship and add new ship(new ship is located in move target)
     * @Param:       Coordinate c, Ship<T> toMove
     * @return:      String that describe problems in ships' placement (if no problem, return null)
     **/
    public String tryMoveShip(Coordinate c, Ship<T> toMove) {
        for(Ship<T> s : myShips){
            if(s.occupiesCoordinates(c)) {
                myShips.remove(s);
                String descr = tryAddShip(toMove);
                if(descr == null){
                    toMove.overwriteCoordinates(s);
                    return null;
                }
                else{
                    tryAddShip(s);
                    return descr;
                }
            }
        }

        return null;
    }

    /**
     * @Description: Get the ship type of ship which contain passed coordinate
     * @Param:       Coordinate c
     * @return:      if ship exist, return ship name; otherwise, return null
     **/
    public String getShipTypeByCoord(Coordinate c) {
        for(Ship<T> s : myShips){
            if(s.occupiesCoordinates(c)){
                return s.getName();
            }
        }
        return null;
    }

    /**
     * @Description: Get the information of Coordinate where based on isSelf (from current player view or enemy's view)
     * @Param:       Coordinate where, boolean isSelf
     * @return:      T information
     **/
    protected T whatIsAt(Coordinate where, boolean isSelf){
        if(isSelf){
            for (Ship<T> s: myShips) {
                if (s.occupiesCoordinates(where)) return s.getDisplayInfoAt(where, isSelf);
            }
        }
        else{
            if(enemyHit.containsKey(where)) return enemyHit.get(where);
            if(enemyMisses.contains(where)) return missInfo;
        }

        return null;
    }

    /**
     * @Description: Get the information of Coordinate where from enemy's view
     * @Param:       Coordinate where
     * @return:      T information
     **/
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    /**
    * @Description: Get the information of Coordinate where from current player view
    * @Param:       Coordinate where
    * @return:      T information
    **/
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * @Description: Fire on Coordinate c, and record it's result in ships or misses
     * @Param:       Coordinate where
     * @return:      Ship<T> that is hit or null
     **/
    public Ship<T> fireAt(Coordinate c){
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(c)){
                s.recordHitAt(c);
                enemyHit.put(c,s.getDisplayInfoAt(c,false));
                enemyMisses.remove(c);
                return s;
            }
        }

        enemyMisses.add(c);
        enemyHit.remove(c);
        return null;
    }

    /**
     * @Description: Check if all ships in board are sunk
     * @Param:
     * @return:      Boolean that indicate whether all ships are sunk
     **/
    public boolean allShipSunk(){
        for(Ship<T> s : myShips){
            if(!s.isSunk()){
                return false;
            }
        }
        return true;
    }
}
