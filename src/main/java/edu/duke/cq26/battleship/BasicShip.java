package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Ship;
import edu.duke.cq26.battleship.interfaces.ShipDisplayInfo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * @program :       battleship
 * @description:    Class of BasicShip
 **/
public abstract class BasicShip<T> implements Ship<T> {
    protected HashMap<Integer, Coordinate> myOrderCoords;
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    /**
    * @Description: Constructor of BasicShip. Constructed by passed Coordinates and ShipDisplayInfo
    * @Param:       Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo
    * @return:
    **/
    public BasicShip(HashMap<Integer,Coordinate> order, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo){
        myOrderCoords = order;
        myPieces = new HashMap<Coordinate, Boolean>();
        for(Coordinate c : order.values()){
            myPieces.put(c,false);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }

    /**
     * @Description: Check whether the passed Coordinate is belong to this ship.
     * @Param:       Coordinate c
     * @throws       IllegalArgumentException if the passed Coordinate is not in this ship
     * @return:
     **/
    protected void checkCoordinateInThisShip(Coordinate c){
        boolean isCInShip = false;
        for(Coordinate shipCoord : myPieces.keySet()){
            if(shipCoord.equals(c)){
                isCInShip = true;
                break;
            }
        }

        if(!isCInShip){
            throw new IllegalArgumentException("edu.duke.cq26.battleship.BasicShip : given Coordinate is not in ship");
        }
    }

    /**
     * @Description: Check whether the passed Coordinate is belong to this ship.
     * @Param:       Coordinate where
     * @return:      Boolean( If where belong to this ship, return true; Otherwise, return false)
     **/
    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        for( Coordinate c : myPieces.keySet()){
            if(where.equals(c)){
                return true;
            }
        }
        return false;
    }

    /**
     * @Description: Check whether this ship is sunk( all Coordinate that belong to this ship are hit)
     * @Param:
     * @return:     Boolean (If sunk, return true; Otherwise, return false)
     **/
    @Override
    public boolean isSunk() {
        for(Map.Entry<Coordinate, Boolean> vo :myPieces.entrySet()){
            if(!vo.getValue()){
                return false;
            }
        }
        return true;
    }

    /**
     * @Description: Record a hit on a Coordinate that belong to this ship
     * @Param:       Coordinate where
     * @return:
     **/
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where,true);
    }

    /**
     * @Description: Check if the passed Coordinate was hit.
     * @Param:       Coordinate where
     * @return:      Boolean (If hit, return true; Otherwise return false)
     **/
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }


    /**
     * @Description: Get display information of the passed Coordinate based on whether from current player's view
     * @Param:       Coordinate where, boolean myShip
     * @return:      T
     **/
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        // TODO this is not right.  We need to look up the hit status of this coordinate
        checkCoordinateInThisShip(where);
        if(myShip){
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        }
        else{
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
        }
    }

    /**
     * @Description: Overwrite this ship's hit situation based on passed old ship
     * @Param:       Ship<T> oldShip
     * @return:
     **/
    @Override
    public void overwriteCoordinates(Ship<T> oldShip){
        HashMap<Integer,Coordinate> oldShipOrderCoords = oldShip.getMyOrderCoords();
        for(Map.Entry<Integer,Coordinate> entry : myOrderCoords.entrySet()){
            myPieces.put(entry.getValue(),oldShip.wasHitAt(oldShipOrderCoords.get(entry.getKey())));
        }
    }

    /**
     * @Description: Get set of coordinates in this ship
     * @Param:
     * @return:      Iterable<Coordinate>
     **/
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    /**
     * @Description: Get ordered map of coordinates in this ship
     * @Param:
     * @return:      HashMap<Integer, Coordinate>
     **/
    @Override
    public HashMap<Integer, Coordinate> getMyOrderCoords() {
        return myOrderCoords;
    }
}
