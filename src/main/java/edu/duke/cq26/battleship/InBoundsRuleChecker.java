package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;

/**
 * @program : battleship
 * @description: Class of checking "out of bound" in ship placement. This class have has-a and is-a relationship.
 **/

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * @Description: Constructor of InBoundsRuleChecker. Constructed by the passed PlacementRuleChecker<T>.
     * @Param:       PlacementRuleChecker<T>
     * @return:
     **/
    protected InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * @Description: Check if the passed ship's occupied region is out of the passed board.
     * @Param:       Ship<T> theShip, Board<T> theBoard
     * @return:      String that describe "out of board" problems of the passed ship's placement. (if no problem, return null)
     **/
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        int rowBound = theBoard.getHeight();
        int colBound = theBoard.getWidth();

        for(Coordinate c : theShip.getCoordinates()){
            if(c.getRow() < 0){
                return "That placement is invalid: the ship goes off the top of the board.";
            }
            if(c.getRow() >= rowBound){
                return "That placement is invalid: the ship goes off the bottom of the board.";
            }
            if(c.getColumn() < 0){
                return "That placement is invalid: the ship goes off the left of the board.";
            }
            if(c.getColumn() >= colBound){
                return "That placement is invalid: the ship goes off the right of the board.";
            }
        }

        return null;
    }
}
