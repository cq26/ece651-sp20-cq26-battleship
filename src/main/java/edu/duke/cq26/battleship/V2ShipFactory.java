package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.AbstractShipFactory;
import edu.duke.cq26.battleship.interfaces.Ship;

/**
 * @program : ece651-sp20-cq26-battleship
 * @description: Class of ship factory in version 2
 **/

public class V2ShipFactory implements AbstractShipFactory<Character> {

    /**
     * @Description: Factory's basic function to create rectangle ships.
     * @Param:       Placement where, int w, int h, char letter, String name
     * @return:      Ship<Character>
     **/
    protected Ship<Character> createRectangleShip(Placement where, int w, int h, char letter, String name){
        Character orientation = where.getOrientation();
        if(orientation != 'H' && orientation != 'V'){
            throw new IllegalArgumentException("Orientation of Placement is illegal");
        }

        int width = w, height = h;
        if(orientation == 'H'){
            width = h;
            height = w;
        }
        return new RectangleShip<Character>(name, where.getCoordinate(), width, height,letter,'*');
    }

    /**
     * @Description: Factory's basic function to create Irregular Battleship ships.
     * @Param:       Placement where, int w, int h, char letter, String name
     * @return:      Ship<Character>
     **/
    protected Ship<Character> createIrregularBattleship(Placement where, char letter, String name){
        Character orientation = where.getOrientation();
        if(orientation != 'U' && orientation != 'R' && orientation != 'D' && orientation != 'L'){
            throw new IllegalArgumentException("Orientation of Placement is illegal");
        }

        return new IrregularBattleShip<>(name, where.getCoordinate(), orientation, letter,'*');
    }

    /**
     * @Description: Factory's basic function to create Irregular Carrier ships.
     * @Param:       Placement where, int w, int h, char letter, String name
     * @return:      Ship<Character>
     **/
    protected Ship<Character> createIrregularCarrier(Placement where, char letter, String name){
        Character orientation = where.getOrientation();
        if(orientation != 'U' && orientation != 'R' && orientation != 'D' && orientation != 'L'){
            throw new IllegalArgumentException("Orientation of Placement is illegal");
        }

        return new IrregularCarrierShip<>(name, where.getCoordinate(), orientation, letter,'*');
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createRectangleShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createIrregularBattleship(where, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createIrregularCarrier(where,  'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createRectangleShip(where, 1, 3, 'd', "Destroyer");
    }
}
