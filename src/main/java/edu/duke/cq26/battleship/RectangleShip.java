package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.ShipDisplayInfo;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @program : battleship
 * @description: Class of RectangleShip
 **/

public class RectangleShip<T> extends BasicShip<T>{
    public final String name;

    /**
     * @Description: Constructor of RectangleShip. Constructed by the passed corresponding information
     * @Param:       String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo
     * @return:
     **/
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeOrderCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * @Description: Constructor of RectangleShip. Constructed by the passed corresponding information
     * @Param:       String name, Coordinate upperLeft, int width, int height, T data, T onHit
     * @return:
     **/
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }

    /**
     * @Description: Constructor of RectangleShip. This is a test constructor
     * @Param:       CCoordinate upperLeft, T data, T onHit
     * @return:
     **/
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }


    /**
     * @Description: Getter of name
     * @Param:
     * @return:      String name
     **/
    public String getName(){
        return name;
    }

    /**
     * @Description: Create a HashMap of Coordinates based on the start Coordinate and size of region
     * @Param:       Coordinate upperLeft, int width, int height
     * @return:      HashMap that includes all <Serial number, Coordinate> in the given region
     **/
    static HashMap<Integer, Coordinate> makeOrderCoords(Coordinate upperLeft, int width, int height){
        HashMap<Integer, Coordinate> orderCoords = new HashMap<>();

        int number = 1;
        for(int row = upperLeft.getRow(); row < upperLeft.getRow() + height; row++){
            for(int col = upperLeft.getColumn(); col < upperLeft.getColumn() + width; col++){
               orderCoords.put(number++, new Coordinate(row,col));
            }
        }

        return orderCoords;
    }

}
