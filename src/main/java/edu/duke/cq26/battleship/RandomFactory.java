package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.AbstractRandomFactory;
import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * @program : ece651-sp20-cq26-battleship
 * @description: Class with function of generating objects with random content
 **/
public class RandomFactory implements AbstractRandomFactory {
    private final Random random;
    private final Board<Character> theBoard;
    private final ArrayList<Character> shipType;
    private final ArrayList<String> choices;

    public RandomFactory(Board<Character> theBoard){
        this.theBoard = theBoard;
        this.random = new Random();
        this.shipType = new ArrayList<>(){
            {
                add('H');   add('V');   add('U');   add('D');   add('L');   add('R');
            }
        };
        this.choices = new ArrayList<>(){
            {
                addAll(Collections.nCopies(10, "F"));   add("M");   add("S");
            }
        };
    }

    /**
     * Make a Coordinate with random row number and random column number
     * @return the random Coordinate
     */
    @Override
    public Coordinate makeRandomCoordinate() {
        int randomRow = random.nextInt(theBoard.getHeight());
        int randomCol = random.nextInt(theBoard.getWidth());

        return new Coordinate(randomRow,randomCol);
    }

    /**
     * Make a Placement with random Coordinate and random Direction
     * @return the random Placement
     */
    @Override
    public Placement makeRandomPlacement() {
        Coordinate c = makeRandomCoordinate();
        return new Placement(c, shipType.get(random.nextInt(shipType.size())));
    }

    /**
     * Make a Choice with random choose from main choice
     * @return the Random Choice
     */
    @Override
    public String makeRandomMainChoice() {
        return choices.get(random.nextInt(1000) % choices.size());
    }
}
