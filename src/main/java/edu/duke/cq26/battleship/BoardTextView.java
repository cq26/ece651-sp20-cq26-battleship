package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Board;

import java.util.function.Function;

/**
 * @program :       battleship
 * @description:    Class of BoardTextView, which is to display the text view of Board
 **/
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * @Description: Constructs a BoardView, given the board it will display.
     * @Param:       toDisplay is the Board to display
     * @throws       IllegalArgumentException if the board is larger than 10x26.
     * @return:
     **/
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    /**
     * @Description: Make the String to display the information in board based on the given function
     * @Param:       Function<Coordinate, Character> getSquareFn
     * @return:      the String that is the description of board
     **/
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder("");
        String sep = "|";

        ans.append(makeHeader());
        for(int i=0; i<toDisplay.getHeight(); i++){
            StringBuilder row = new StringBuilder("");
            char numRow = (char) (i + 'A');

            /* header of row */
            row.append(numRow);
            row.append(" ");

            /* body of row */
            for(int j=0; j<toDisplay.getWidth(); j++){
                if(j != 0)  row.append(sep);

                Character shipType = getSquareFn.apply(new Coordinate(i,j));
                if(shipType != null)    row.append(shipType);
                else                    row.append(" ");
            }

            /* tail of row*/
            row.append(" ");
            row.append(numRow);
            row.append("\n");

            ans.append(row);
        }
        ans.append(makeHeader());
        return ans.toString(); //this is a placeholder for the moment
    }

    /**
     * @Description: This makes the header line, e.g. 0|1|2|3|4\n.
     * @Param:
     * @return:      the String that is the header line for the given board
     **/
    public String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * @Description: Display board from current player's view
     * @Param:
     * @return:      the String that is board from current player's view
     **/
    public String displayMyOwnBoard() {
        return displayAnyBoard(toDisplay::whatIsAtForSelf);
    }

    /**
     * @Description: Display board from enemy's view
     * @Param:
     * @return:      the String that is board from enemy's view
     **/
    public String displayEnemyBoard(){
        return displayAnyBoard(toDisplay::whatIsAtForEnemy);
    }

    /**
     * @Description: Display own board and enemy's board from current player's view
     * @Param:
     * @return:      the String that contains two board
     **/
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String myViewOwn = displayMyOwnBoard();
        String myViewEnemy = enemyView.displayEnemyBoard();

        String [] myViewOwnLines = myViewOwn.split("\n");
        String [] myViewEnemyLines = myViewEnemy.split("\n");

        /* Build of result's head */
        StringBuilder head = new StringBuilder();
        head.append(" ".repeat(2)).append(myHeader).append(" ".repeat(2*toDisplay.getWidth()+29 - myHeader.length())).append(enemyHeader).append("\n");

        /* Build of result's body */
        StringBuilder body = new StringBuilder();
        for(int i=0; i<myViewOwnLines.length; i++){
            body.append(myViewOwnLines[i]);
            if(i == 0 || i == myViewOwnLines.length - 1){
                body.append(" ".repeat(28));
            }
            else{
                body.append(" ".repeat(26));
            }
            body.append(myViewEnemyLines[i]).append("\n");
        }

        /* bind head & body */
        return head.toString() + body.toString();
    }
}
