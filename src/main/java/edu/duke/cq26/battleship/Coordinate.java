package edu.duke.cq26.battleship;

import java.util.Locale;
/**
 * @program :       battleship
 * @description:    Class of Coordinate in the Board
 **/
public class Coordinate {
    private final int row;
    private final int column;

    /**
     * @Description: Constructor of Coordinate. Constructed by the passed row and the passed column
     * @Param:       int row, int column
     * @return:
     **/
    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * @Description: Constructor of Coordinate. Constructed by description string of position, like "A0".
     * @Param:       String descr
     * @throws       IllegalArgumentException if the description's length is not 2 , row number is out of A-T, or column number is out of 0-9
     * @return:
     **/
    public Coordinate(String descr){
        if(descr.length() != 2){
            throw new IllegalArgumentException("edu.duke.cq26.battleship.Coordinate's descriptor must be 2\n");
        }

        descr = descr.toUpperCase(Locale.ROOT);

        int r = descr.charAt(0);
        int c = descr.charAt(1);

        if(r < 'A' || r > 'Z'){
            throw new IllegalArgumentException("edu.duke.cq26.battleship.Coordinate's row must be A-Z\n");
        }
        if(c < '0' || c > '9'){
            throw new IllegalArgumentException("edu.duke.cq26.battleship.Coordinate's column must be 0-9\n");
        }
        this.row = r - 'A';
        this.column = c - '0';
    }

    /** getters and setters **/
    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }


    /**
     * @Description: Check if the passed object is an Object of Coordinate, and check if it has same value with "this".
     * @Param:       Object o
     * @return:      Boolean that indicate if the passed Object is equal to "this".
     **/
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    /**
     * @Description: Describe this Coordinate in String.
     * @Param:
     * @return:      String that describe this Coordinate
     **/
    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }

    /**
     * @Description: Calculate the hashcode of this.toString()
     * @Param:
     * @return:      Int of hashcode
     **/
    @Override
    public int hashCode() {
        return toString().hashCode();
    }


}
