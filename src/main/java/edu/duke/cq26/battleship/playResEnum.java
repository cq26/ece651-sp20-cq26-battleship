package edu.duke.cq26.battleship;

/**
 * @program :       battleship
 * @description:    Enumeration Class of playing result of each player's operation
 **/

public enum playResEnum {
    SUCCESS,        /* Play successfully */
    FAIL,           /* Play successfully */
    BACKTOMAIN     /* Back to main manual */
}
