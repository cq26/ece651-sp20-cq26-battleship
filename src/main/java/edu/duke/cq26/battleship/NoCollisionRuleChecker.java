package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;

import java.util.HashSet;
import java.util.Set;

/**
 * @program : battleship
 * @description: Class of checking collision in ship placement. This class have has-a and is-a relationship.
 **/

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T>{

    /**
     * @Description: Constructor of NoCollisionRuleChecker. Constructed by the passed PlacementRuleChecker<T>.
     * @Param:       PlacementRuleChecker<T>
     * @return:
     **/
    protected NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * @Description: Check if the passed ship's occupied region is overlapped with other ships in the board.
     * @Param:       Ship<T> theShip, Board<T> theBoard
     * @return:      String that describe "collision" problems of the passed ship's placement. (if no problem, return null)
     **/
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate> shipCoords = theShip.getCoordinates();
        for(Coordinate c : shipCoords){
            if(theBoard.whatIsAtForSelf(c) != null){
                return "That placement is invalid: the ship overlaps another ship.";
            }
        }

        return null;
    }
}
