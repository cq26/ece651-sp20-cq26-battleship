package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @program : battleship
 * @description: Class of Test of TextPlayer
 **/

public class TextPlayerTest {
    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

    private TextPlayer createTextPlayerFactory2(Board<Character> board, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        V2ShipFactory shipFactory = new V2ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }

    @Test
    void test_read_placement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }


    @Test
    void test_one_replacement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\n", bytes);
        String shipName = "Destroyer";

        String prompt = "Player A where do you want to place a Destroyer?\n";
        String expectedDis = "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | |d| | | | | | |  B\n" +
                "C  | |d| | | | | | |  C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";

        player.doOnePlacement(shipName,player.shipCreationFns.get(shipName));

        assertEquals(prompt + expectedDis,bytes.toString());
    }

    @Test
    void test_read_coordinate() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2\nC8\nA4\n", bytes);

        String prompt = "Please enter a location for a ship:";
        Coordinate[] expected = new Coordinate[3];
        expected[0] = new Coordinate(1,2);
        expected[1] = new Coordinate(2,8);
        expected[2] = new Coordinate(0,4);

        for (int i = 0; i < expected.length; i++) {
            Coordinate p = player.readCoordinate(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }

    @Test
    void test_play_one_round() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer playerA = createTextPlayer(10, 20, "A1\n", bytes);
        TextPlayer playerB = createTextPlayer(10, 20, "A2\n", bytes);

        /* enemy's board */
        V1ShipFactory shipFactory = new V1ShipFactory();
        playerB.getTheBoard().tryAddShip(shipFactory.makeDestroyer(new Placement("B2V")));

        String prompt = "Player A where do you want to place a Destroyer?\n";
        String expectedDis = "  0|1|2|3|4|5|6|7|8|9\n" +
                "A  | | | | | | | | |  A\n" +
                "B  | |d| | | | | | |  B\n" +
                "C  | |d| | | | | | |  C\n" +
                "D  | |d| | | | | | |  D\n" +
                "E  | | | | | | | | |  E\n" +
                "F  | | | | | | | | |  F\n" +
                "G  | | | | | | | | |  G\n" +
                "H  | | | | | | | | |  H\n" +
                "I  | | | | | | | | |  I\n" +
                "J  | | | | | | | | |  J\n" +
                "K  | | | | | | | | |  K\n" +
                "L  | | | | | | | | |  L\n" +
                "M  | | | | | | | | |  M\n" +
                "N  | | | | | | | | |  N\n" +
                "O  | | | | | | | | |  O\n" +
                "P  | | | | | | | | |  P\n" +
                "Q  | | | | | | | | |  Q\n" +
                "R  | | | | | | | | |  R\n" +
                "S  | | | | | | | | |  S\n" +
                "T  | | | | | | | | |  T\n" +
                "  0|1|2|3|4|5|6|7|8|9\n";

        assertEquals(expectedDis,playerB.getView().displayMyOwnBoard());
//        assertEquals("You missed!",playerA.playOneTurn(playerB.getTheBoard(),playerB.getView(),playerB.getName()));
    }

    @Test
    void test_play_fire() throws IOException {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);
        b2.tryAddShip(dst);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer playerA = createTextPlayerFactory2(b1, "B2\n", bytes);
        TextPlayer playerB = createTextPlayerFactory2(b1,  "quit\n", bytes);
        TextPlayer playerC = createTextPlayerFactory2(b1,  "Bsdsd\n", bytes);

        assertEquals(playerA.playFire(b2),playResEnum.SUCCESS);
        assertEquals(playerB.playFire(b2),playResEnum.BACKTOMAIN);
        assertEquals(playerC.playFire(b2),playResEnum.FAIL);
    }

    @Test
    void test_play_move() throws IOException {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);
        b1.tryAddShip(dst);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer playerA = createTextPlayerFactory2(b1,"B2\nB3H\n", bytes);

        assertEquals(playerA.playMove(),playResEnum.SUCCESS);
    }

    @Test
    void test_play_sonar() throws IOException {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);
        b1.tryAddShip(dst);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer playerA = createTextPlayerFactory2(b2,"B2\n", bytes);
        TextPlayer playerB = createTextPlayerFactory2(b2,"quit\n", bytes);
        TextPlayer playerC = createTextPlayerFactory2(b2,"Baewr\n", bytes);

        assertEquals(playerA.playSonar(b1),playResEnum.SUCCESS);
        assertEquals(playerB.playSonar(b1),playResEnum.BACKTOMAIN);
        assertEquals(playerC.playSonar(b1),playResEnum.FAIL);
    }

    @Test
    void test_play_one_turn() throws IOException {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);
        b1.tryAddShip(dst);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer playerA = createTextPlayerFactory2(b2,"F\nZ2\nB2\n", bytes);
        TextPlayer playerB = createTextPlayerFactory2(b2,"S\nZ2\nB2\n", bytes);

        playerA.playOneTurn(b1,new BoardTextView(b1),"A");
        playerB.playOneTurn(b1,new BoardTextView(b1),"B");
    }


}
