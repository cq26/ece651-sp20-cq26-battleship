package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : battleship
 * @description: Class of V1 shipFactory Test
 **/

public class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs){
        assertEquals(expectedName,testShip.getName());
        for(Coordinate c : expectedLocs){
            assertTrue(testShip.occupiesCoordinates(c));
            assertEquals(expectedLetter,testShip.getDisplayInfoAt(c,true));
        }
//        assertTrue(testShip.isSunk());
    }

    @Test
    void test_make_ships(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
        checkShip(dst1, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));

        V1ShipFactory v1ShipFactory2 = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst2 = v1ShipFactory2.makeSubmarine(v1_2);
        checkShip(dst2, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

        V1ShipFactory v1ShipFactory3 = new V1ShipFactory();
        Placement v1_3 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst3 = v1ShipFactory3.makeBattleship(v1_3);
        checkShip(dst3, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));

        V1ShipFactory v1ShipFactory4 = new V1ShipFactory();
        Placement v1_4 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst4 = v1ShipFactory4.makeCarrier(v1_4);
        checkShip(dst4, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)
                                                                 , new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));
    }

    @Test
    void test_invalid_placement(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'A');
        assertThrows(IllegalArgumentException.class, () -> v1ShipFactory.makeDestroyer(v1_1));
    }
}

