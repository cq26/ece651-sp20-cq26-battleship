package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BoardTextViewTest {
    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<>(w, h, 'X');
        b1.tryAddShip((new RectangleShip<>(new Coordinate(0,1), 's', '*')));
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_3by2(){
        String expectedHeader = "  0|1|2\n";
        String expectedBody = "A  |s|  A\n"
                            + "B  | |  B\n";
        emptyBoardHelper(3,2,expectedHeader,expectedBody);
    }

    @Test
    public void test_display_empty_3by5(){
        String expectedHeader = "  0|1|2\n";
        String expectedBody = "A  |s|  A\n"
                            + "B  | |  B\n"
                            + "C  | |  C\n"
                            + "D  | |  D\n"
                            + "E  | |  E\n";
        emptyBoardHelper(3,5,expectedHeader,expectedBody);
    }

    @Test
    public void test_display_empty_2by2() {
        String expectedHeader = "  0|1\n";
        String expectedBody = "A  |s A\n"
                            + "B  |  B\n";
        emptyBoardHelper(2,2,expectedHeader,expectedBody);
    }

    @Test
    public void test_invalid_board_size() {
        ArrayList<Ship<Character>> myShips = new ArrayList<>();
        Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
        //you should write two assertThrows here
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));

    }

    @Test
    void test_display_board_view_for_enemy(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 0), 'H');
        Ship<Character> subm = v1ShipFactory.makeSubmarine(v1);
        BoardTextView view = new BoardTextView(b1);

        b1.tryAddShip(subm);

        String myView =
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s|s| |  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";
        //make sure we laid things out the way we think we did.
        assertEquals(myView, view.displayMyOwnBoard());

        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(1,0);

        b1.fireAt(c1);
        b1.fireAt(c2);

        String enemyView =
                        "  0|1|2|3\n" +
                        "A  | | |  A\n" +
                        "B s| |X|  B\n" +
                        "C  | | |  C\n" +
                        "  0|1|2|3\n";

        assertEquals(enemyView, view.displayEnemyBoard());

    }

    @Test
    void test_display_my_board_with_enemy_board(){
        /* playerA's board */
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 0), 'H');
        Ship<Character> subm = v1ShipFactory.makeSubmarine(v1);
        BoardTextView view = new BoardTextView(b1);

        b1.tryAddShip(subm);

        /* playerB's board */
        BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(4, 3, 'X');
        V1ShipFactory v1ShipFactory2 = new V1ShipFactory();
        Placement v2 = new Placement(new Coordinate(2, 0), 'H');
        Ship<Character> subm2 = v1ShipFactory2.makeSubmarine(v2);
        BoardTextView enemyView = new BoardTextView(b2);

        b2.tryAddShip(subm2);
        b2.fireAt(new Coordinate(2, 0));
        b2.fireAt(new Coordinate(0, 0));

        String MyResult = "  Your Ocean                           Player B's Ocean\n" +
                        "  0|1|2|3                              0|1|2|3\n" +
                        "A  | | |  A                          A X| | |  A\n" +
                        "B s|s| |  B                          B  | | |  B\n" +
                        "C  | | |  C                          C s| | |  C\n" +
                        "  0|1|2|3                              0|1|2|3\n";

        String EneResult = "  Your Ocean                           Player B's Ocean\n" +
                            "  0|1|2|3                              0|1|2|3\n" +
                            "A  | | |  A                          A  | | |  A\n" +
                            "B  | | |  B                          B  | | |  B\n" +
                            "C *|s| |  C                          C  | | |  C\n" +
                            "  0|1|2|3                              0|1|2|3\n";

        assertEquals(MyResult,view.displayMyBoardWithEnemyNextToIt(enemyView,"Your Ocean", "Player B's Ocean"));
        assertEquals(EneResult,enemyView.displayMyBoardWithEnemyNextToIt(view,"Your Ocean", "Player B's Ocean"));
    }

}
