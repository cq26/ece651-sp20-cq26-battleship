package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : ece651-sp20-cq26-battleship
 * @description: Class of tests of Irregular ships ( carrier and battileship )
 **/

public class IrregularShipTest {
    @Test
    void test_constructor_initial_correct(){
        IrregularBattleShip irregularBattleShipUp = new IrregularBattleShip("Battleship",new Coordinate(1,2),'U','b','*');
        HashMap<Integer,Coordinate> expectedUp = new HashMap<>();
        expectedUp.put(1,new Coordinate(1,3));
        expectedUp.put(2,new Coordinate(2,2));
        expectedUp.put(3,new Coordinate(2,3));
        expectedUp.put(4,new Coordinate(2,4));

        assertEquals(expectedUp,irregularBattleShipUp.getMyOrderCoords());

        IrregularBattleShip irregularBattleShipRight = new IrregularBattleShip("Battleship",new Coordinate(1,2),'R','b','*');
        HashMap<Integer,Coordinate> expectedRight = new HashMap<>();
        expectedRight.put(1,new Coordinate(2,3));
        expectedRight.put(2,new Coordinate(1,2));
        expectedRight.put(3,new Coordinate(2,2));
        expectedRight.put(4,new Coordinate(3,2));

        assertEquals(expectedRight,irregularBattleShipRight.getMyOrderCoords());

        IrregularBattleShip irregularBattleShipDown = new IrregularBattleShip("Battleship",new Coordinate(1,2),'D','b','*');
        HashSet<Coordinate> expectedDown = new HashSet<>();
        expectedDown.add(new Coordinate(1,2));
        expectedDown.add(new Coordinate(1,3));
        expectedDown.add(new Coordinate(1,4));
        expectedDown.add(new Coordinate(2,3));

        assertEquals(expectedDown,irregularBattleShipDown.getCoordinates());

        IrregularBattleShip irregularBattleShipLeft = new IrregularBattleShip("Battleship",new Coordinate(1,2),'L','b','*');
        HashSet<Coordinate> expectedLeft = new HashSet<>();
        expectedLeft.add(new Coordinate(1,3));
        expectedLeft.add(new Coordinate(2,2));
        expectedLeft.add(new Coordinate(2,3));
        expectedLeft.add(new Coordinate(3,3));

        assertEquals(expectedLeft,irregularBattleShipLeft.getCoordinates());

        IrregularCarrierShip irregularCarrierShipLeft = new IrregularCarrierShip("Carrier", new Coordinate(1,2),'L','c','*');
        HashSet<Coordinate> expectedLeftCarrier = new HashSet<>();
        expectedLeftCarrier.add(new Coordinate(1,3));
        expectedLeftCarrier.add(new Coordinate(1,2));
        expectedLeftCarrier.add(new Coordinate(1,4));
        expectedLeftCarrier.add(new Coordinate(2,4));
        expectedLeftCarrier.add(new Coordinate(2,5));
        expectedLeftCarrier.add(new Coordinate(2,6));

        assertEquals(expectedLeftCarrier,irregularCarrierShipLeft.getCoordinates());

        IrregularCarrierShip irregularCarrierShipUp = new IrregularCarrierShip("Carrier", new Coordinate(1,2),'U','c','*');
        HashSet<Coordinate> expectedCarrierUp = new HashSet<>();
        expectedCarrierUp.add(new Coordinate(1,2));
        expectedCarrierUp.add(new Coordinate(2,2));
        expectedCarrierUp.add(new Coordinate(3,2));
        expectedCarrierUp.add(new Coordinate(3,3));
        expectedCarrierUp.add(new Coordinate(4,3));
        expectedCarrierUp.add(new Coordinate(5,3));

        assertEquals(expectedCarrierUp,irregularCarrierShipUp.getCoordinates());

        IrregularCarrierShip irregularCarrierShipRight = new IrregularCarrierShip("Carrier", new Coordinate(1,2),'R','c','*');
        HashSet<Coordinate> expectedCarrierRight = new HashSet<>();
        expectedCarrierRight.add(new Coordinate(2,2));
        expectedCarrierRight.add(new Coordinate(2,3));
        expectedCarrierRight.add(new Coordinate(2,4));
        expectedCarrierRight.add(new Coordinate(1,4));
        expectedCarrierRight.add(new Coordinate(1,5));
        expectedCarrierRight.add(new Coordinate(1,6));

        assertEquals(expectedCarrierRight,irregularCarrierShipRight.getCoordinates());

        IrregularCarrierShip irregularCarrierShipDown = new IrregularCarrierShip("Carrier", new Coordinate(1,2),'D','c','*');
        HashSet<Coordinate> expectedCarrierDown = new HashSet<>();
        expectedCarrierDown.add(new Coordinate(1,3));
        expectedCarrierDown.add(new Coordinate(2,3));
        expectedCarrierDown.add(new Coordinate(3,3));
        expectedCarrierDown.add(new Coordinate(3,2));
        expectedCarrierDown.add(new Coordinate(4,2));
        expectedCarrierDown.add(new Coordinate(5,2));

        assertEquals(expectedCarrierDown,irregularCarrierShipDown.getCoordinates());
    }

    @Test
    void test_get_name(){
        IrregularBattleShip irregularBattleShipUp = new IrregularBattleShip("Battleship",new Coordinate(1,2),'U','b','*');
        IrregularCarrierShip irregularCarrierShipDown = new IrregularCarrierShip("Carrier", new Coordinate(1,2),'D','c','*');

        assertEquals("Battleship",irregularBattleShipUp.getName());
        assertEquals("Carrier",irregularCarrierShipDown.getName());
    }
}
