package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class BattleShipBoardTest {
    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected){
        for(int i = 0; i < b.getHeight(); i++){
            for(int j = 0; j < b.getWidth(); j++){
                assertEquals(b.whatIsAtForSelf(new Coordinate(i,j)), expected[i][j]);
            }
        }
    }

    public void test_ships_in_board(int width, int height){
        BattleShipBoard<Character> b = new BattleShipBoard<Character>(width, height, 'X');
        Character[][] expected = new Character[height][width];
        for(int i=0 ; i<b.getHeight(); i++){
            Arrays.fill(expected[i],null);
        }

        /* Firstly check no ships in Board */
        checkWhatIsAtBoard(b,expected);

        /* Secondly add ship into Board and check correction */
        for(int i=0; i<b.getHeight(); i++){
            for(int j=0; j<b.getWidth(); j++){
                b.tryAddShip(new RectangleShip<Character>(new Coordinate(i,j), 's', '*'));
                expected[i][j] = 's';
                checkWhatIsAtBoard(b,expected);
            }
        }
    }

    @Test
    public void test_ships_in_board_all(){
        test_ships_in_board(2,2);
        test_ships_in_board(3,2);
        test_ships_in_board(4,5);
    }

    @Test
    public void test_width_and_height() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        assertEquals(20, b1.getHeight());
    }

    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(20, -8, 'X'));
    }

    @Test
    void test_fire_at(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);

        b1.tryAddShip(dst);

        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        Coordinate c4 = new Coordinate(4,2);

        assertSame(dst,b1.fireAt(c1));
        assertTrue(dst.wasHitAt(c1));
        assertFalse(dst.isSunk());

        assertSame(dst,b1.fireAt(c2));
        assertTrue(dst.wasHitAt(c2));
        assertFalse(dst.isSunk());

        assertSame(null,b1.fireAt(c4));
        assertTrue(b1.getEnemyMisses().contains(c4));

        assertSame(dst,b1.fireAt(c3));
        assertTrue(dst.wasHitAt(c3));
        assertTrue(dst.isSunk());
    }

    @Test
    void test_what_is_for_enemy(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);

        b1.tryAddShip(dst);

        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        Coordinate c4 = new Coordinate(4,2);

        b1.fireAt(c1);
        b1.fireAt(c4);

        assertEquals('X',b1.whatIsAtForEnemy(c4));
        assertEquals('d',b1.whatIsAtForEnemy(c1));
        assertNull(b1.whatIsAtForEnemy(c2));
    }

    @Test
    void test_all_ship_sunk(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);

        b1.tryAddShip(dst);
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        Coordinate c4 = new Coordinate(4,2);

        b1.fireAt(c1);
        assertFalse(b1.allShipSunk());
        b1.fireAt(c4);
        assertFalse(b1.allShipSunk());
        b1.fireAt(c2);
        assertFalse(b1.allShipSunk());
        b1.fireAt(c3);
        assertTrue(b1.allShipSunk());
    }

    @Test
    void test_move_ship(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Placement v2 = new Placement(new Coordinate(2, 3), 'V');
        Placement v3 = new Placement(new Coordinate(19, 3), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);

        b1.tryAddShip(dst);
        b1.fireAt(new Coordinate(1,2));
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v2);
        b1.tryMoveShip(new Coordinate(1,2), dst2);
        assertTrue(dst2.wasHitAt(new Coordinate(2,3)));

        Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v3);
        b1.tryMoveShip(new Coordinate(2,3), dst3);
        assertTrue(dst2.wasHitAt(new Coordinate(2,3)));

        assertNull(b1.tryMoveShip(new Coordinate(10,3), dst2));
    }

    @Test
    void test_get_ship_type_by_coordinate(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);
        b1.tryAddShip(dst);

        assertEquals(b1.getShipTypeByCoord(new Coordinate(1,2)),"Destroyer");
        assertNull(b1.getShipTypeByCoord(new Coordinate(5,2)));

    }

    @Test
    void test_get_ships(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        Placement v1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = v1ShipFactory.makeDestroyer(v1);
        b1.tryAddShip(dst);

        assertTrue(b1.getMyShips().contains(dst));
    }

}
