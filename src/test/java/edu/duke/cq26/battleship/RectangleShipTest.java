package edu.duke.cq26.battleship;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : battleship
 * @description: Test cases for RectangleShip
 **/

public class RectangleShipTest {
//    @Test
//    void test_make_Coords(){
//        RectangleShip rectangleShip = new RectangleShip();
//        HashSet<Coordinate> coordinateHashSet = new HashSet<>();
//        coordinateHashSet.add(new Coordinate(1,2));
//        coordinateHashSet.add(new Coordinate(2,2));
//        coordinateHashSet.add(new Coordinate(3,2));
//
//        assertEquals(coordinateHashSet, rectangleShip.makeCoords(new Coordinate(1,2),1,3));
//    }

    @Test
    void test_recship_constructor_initial_correct(){
        RectangleShip rectangleShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        assertTrue(rectangleShip.occupiesCoordinates(new Coordinate(1, 2)));
        assertTrue(rectangleShip.occupiesCoordinates(new Coordinate(2, 2)));
        assertTrue(rectangleShip.occupiesCoordinates(new Coordinate(3, 2)));
        assertFalse(rectangleShip.occupiesCoordinates(new Coordinate(4, 1)));
        assertFalse(rectangleShip.occupiesCoordinates(new Coordinate(0, 2)));
    }

    @Test
    void test_hit_record_and_hit_check(){
        RectangleShip rectangleShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        rectangleShip.recordHitAt(c1);
        assertTrue(rectangleShip.wasHitAt(c1));
        assertFalse(rectangleShip.wasHitAt(c2));
        assertFalse(rectangleShip.wasHitAt(c3));
    }

    @Test
    void test_is_sunk(){
        RectangleShip rectangleShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        rectangleShip.recordHitAt(c1);
        rectangleShip.recordHitAt(c2);
        rectangleShip.recordHitAt(c3);
        assertTrue(rectangleShip.isSunk());

        RectangleShip rectangleShip2 = new RectangleShip(new Coordinate(1,2),'s', '*');
        assertFalse(rectangleShip2.isSunk());
    }

    @Test
    void test_get_display_info_at(){
        RectangleShip rectangleShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(2,2);
        Coordinate c3 = new Coordinate(3,2);
        rectangleShip.recordHitAt(c1);
        rectangleShip.recordHitAt(c2);
        assertEquals('*',rectangleShip.getDisplayInfoAt(c1,true));
        assertEquals('*',rectangleShip.getDisplayInfoAt(c2,true));
        assertEquals('s',rectangleShip.getDisplayInfoAt(c3,true));
    }

    @Test
    void test_check_coord_out_of_bound(){
        RectangleShip rectangleShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        Coordinate c1 = new Coordinate(4,5);
        Coordinate c2 = new Coordinate(0,0);
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.recordHitAt(c1));
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.recordHitAt(c2));
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.wasHitAt(c1));
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.wasHitAt(c2));
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.getDisplayInfoAt(c1,true));
        assertThrows(IllegalArgumentException.class, () -> rectangleShip.getDisplayInfoAt(c2,true));
    }

    @Test
    void test_get_coordinates(){
        RectangleShip rectangleShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        HashSet<Coordinate> expected = new HashSet<>();
        expected.add(new Coordinate(1,2));
        expected.add(new Coordinate(2,2));
        expected.add(new Coordinate(3,2));
        assertEquals(expected,rectangleShip.getCoordinates());
    }

    @Test
    void test_overwirte_coordinates(){
        RectangleShip oldShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        RectangleShip newShip = new RectangleShip("submarine",new Coordinate(1,2),1,3,'s', '*');
        oldShip.recordHitAt(new Coordinate(1,2));
        newShip.overwriteCoordinates(oldShip);
        assertTrue(oldShip.wasHitAt(new Coordinate(1,2)));
    }
}
